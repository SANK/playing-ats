

// Getting Started --------------------------------------------------------------- ATS

// this is a comment
/* so is this */
(* and this *)

#include "share/atspre_staload.hats" // pastes the code directly in before compilation

val _ = print 4 // displays the number 4 which is of type int

val _ = showtype 4 // displays the type of 4 in the typecheck log output. Useful for debugging

val _ = print "Hello, world!" // strings can be printed too

val _ : (int, string, double) = (1, "egg", 1e9) // tuples allow for storing different types side by side
                                                // the tuple after the : and before = is the type

val square = lam (x: int): int => // functions can be defined with lam taking a list of arguments and
  x * x                           // the return type

val multiply_int_int = lam (x:int, y:int): int => x * y

val isleap = lam (x: int): bool => // boolean operators for and (&&), or (||), not (!=), and equal (=)
  (x mod 400) = 0 || (x mod 4) = 0 && (x mod 100) != 0

val sum_from_to_0 = fix loop(x: int): int => // recursive functions use fix instead of lambda and must
  if x <= 1 then 1 else x + loop(x-1)        // be given a name for marking the call to self

fn cube (x: int): int = // non recursive functions can also be defined using fn
  x * x * x

fun factorial (x: int): int = // recursive functions use fun instead
  if x >= 1 then 1 else x * factorial(x-1)

fn print_from_0_to_10 () : void = // void types signify the function doesn't return anything which is
let                               // the norm for functions which modify resources
  fun loop (x: int): void =             // helper functions can be defined inside the body of a function
    if x < 10 then (print x; loop(x+1)) // using a let block which allows definitions between let and in
in                                      // while in to end is the let body
  loop(0) // we also must evaluate the expression
end // [end of the let block and print_from_0_to_10]

fn identity {a:t@ype} (a: a): a = a // We can have generic functions too where it works for all types
                                    // where {a:t@ype} means for all types a

fn const {a,b:t@ype} (a: a): b -<cloref0> a = // we can also have generic a function which creates a
  lam (b) => a                                // function ignoring it's only argument and returning
                                              // the value given by the parent function

fn const_curried {a,b:t@ype} (a: a) (b: b): a = // by making use of currying we can simplify the above
  a

datatype Food = Apple | Tomato | Fish | Rice // abstract data types can be deifned with datatype
                                             // in this case we define an enumerable type with 4
                                             // items (ADTs)

fn eq_Food_Food (item1: Food, item2: Food): bool = // we can use ADTs as regular types
  case (item1, item2) of           // pattern matching can be used with the case of statement
  | (Apple (), Apple ())   => true // to match on an ADT we need to use () after the constrcutor otherwise
  | (Tomato (), Tomato ()) => true // it will be treated as a variable binding.
  | (Fish (), Fish ())     => true
  | (Rice (), Rice ())     => true
  | (_, _)                 => false // to match any value without binding it we use _ to discard the values
                                    // this will always match

overload = with eq_Food_Food // we can overload operators with functions we've defined to avoid having to
                              // have a new symbol for each type. In this case we bind equality

symintr .equal                    // we can also introduce . syntax and have it seem more like a property
overload .equal with eq_Food_Food // or method of the type

val _ = print (Apple = Tomato) // comparison of two items is done with the equality operator

val _ = print (Apple.equal(Apple)) // since we defined dot syntax we can use it aswell

val (left, right) = (1, 2) // we can deconstruct a tuple by matching it on the left hand side by
                           // providing names for each field

fn twice {a:t@ype} (f: a -> a, value: a): a = // functions can take functions as arguemnts
  f(f(value))

fn make_adder (increment: int): int -<cloref0> int = // functions can also return functions
  lam (x: int) => x + increment // here `int -<cloref0> int` is the type of the function we
                                // return. Since the function takes a value from the parent
                                // functions scope, it is a closure or a function bundled with
                                // it's environment.

datatype Maybe (a:t@ype) = // ADTs can also be used to implement nullary / option types where
  | Just of a              // there might be Just a result or Nothing
  | Nothing

fn safe_div (a: int, b: int): Maybe int =
  if b = 0
     then Nothing
     else Just (a/b)

typedef complex = @{ real=int, imaginary=int } // records are defined with @{} and are similar to
                                               // C structs. typedef binds a name to a type

fn complex_map_both (c0: complex, fopr: int -> int, fopl: int -> int): complex =
let
  val @{ real= r, imaginary= i } = c0 // records can also be deconstructed by matching using val
  val new_r = fopr(r) // we can bind the immediate computations result to a value
  val new_i = fopr(i)
in
  @{ real= new_r, imaginary= new_i }
end

fn complex_map_real (c0: complex, fop: int -> int): complex =
  complex_map_both (c0, fop, identity) // identity is the general function of "do nothing" and always
                                       // returns the value given

fn complex_map_imaginary (c0: complex, fop: int -> int): complex =
  complex_map_both (c0, identity, fop)

fn complex (real: int, imaginary: int):<> complex = // introducing a constructor makes it much easier to
  @{ real= real, imaginary= imaginary }             // build values without having to know the underlying
                                                    // implementation

symintr .map_real .map_imaginary .map_both
overload .map_real      with complex_map_real      // we can also overload operators with functions
overload .map_imaginary with complex_map_imaginary // taking functions
overload .map_both      with complex_map_both

fn plus_complex_complex (c0: complex, c1: complex): complex =          // we can also use the dot syntax to
  complex(c0.real + c1.real, c0.imaginary + c1.imaginary) // directly access record members
overload + with plus_complex_complex // overloading can be applied for any operator

fn subtract_complex_complex (c0: complex, c1: complex): complex =
  complex(c0.real - c1.real, c0.imaginary - c1.imaginary)
overload - with subtract_complex_complex

fn multiply_complex_complex (c0: complex, c1:complex): complex =
let // multiplication of complex numbers is defined by (a+bi)(c+di) = (ac-bd)+(bc+ad)i
  val ac = c0.real * c1.real           // we can use val definitions to work out our result
  val bd = c0.imaginary * c1.imaginary // in steps much like we do when working it out by hand
  val bc = c0.imaginary * c1.real
  val ad = c0.real * c1.imaginary
in
  complex(ac-bd, bc+ad) // (ac-bd)+(bc+ad)i
end
overload * with multiply_complex_complex

fn divide_complex_complex    // division of complex numbers is defined by
  (c0: complex, c1: complex) // (a+bi)/(c+di) = ((ac+bd)/(c^2+d^2)) + ((bc-ad)/(c^2+d^2))i
  :<> complex =
let
  val ac = c0.real * c1.real           // thus we get the product of both real values
  val bd = c0.imaginary * c1.imaginary // and their imaginary parts
  val c2 = c1.real ** 2                // square the second number's real
  val d2 = c1.imaginary ** 2           // and imaginary part
  val bc = c0.imaginary * c1.real      // then get the product of the first imaginary and the second real
  val ad = c0.real * c1.imaginary      // and the product of the first real and second imaginary
in
  complex((ac+bd)/(c2+d2), (bc-ad)/(c2+d2)) //  a + bi     ac + bd     bc - ad
end                                         //  ------  =  -------  +  ------- i
overload / with divide_complex_complex      //  c + di     c2 + d2     c2 + d2

fn eq_complex_complex (c0: complex, c1: complex): bool = // comparison of imaginary numbers comprises of
  (c0.real = c1.real) && (c0.imaginary = c1.imaginary)   // just comparing the two parts
overload = with eq_complex_complex

fn neq_complex_complex (c0: complex, c1: complex): bool = // inverse comparison can be done with negating
  not (c0 = c1)                                           // the result
overload != with neq_complex_complex

datatype List (a:t@ype) = // ADTs can also handle recursive structures such as lists
  | Nil                 // Nil is an empty list
  | Cons of (a, List a) // Cons adds an item to the head of a list

fun map {a,b:t@ype} (xs: List a, fop: a -> b): List b = // note that a and b can be of any type
  case xs of            // we match on the constrctors where Nil means we're done and
  | Nil ()       => Nil // Cons means we have more work to do
  | Cons (x, xs) => Cons (fop(x), map (xs, fop)) // a List is really just nested Cons
                                                 // with a Nil at the end like so:
                                                 //   Cons(3, Cons(2, Cons(1, Nil)))
symintr .map
overload .map with map // convenience

fun incorrect_sum_from_to_0 (n: int): int = // consider this clearly correct function
  if n >= 0 // it will terminate with the value 0 for all positive and continue until
     then 0 // integer overflow if passed a negative number. It's a simple mistake but
     else n + incorrect_sum_from_to_0 (n-1) // can be easy to make and hard to find

fun correct_sum_from_to_0 // refinement types force us to write a correct implementation
  {n:int | n >= 0} // for all possible values of n that are equal or above 0
  .<n>.            // where n is strictly decreasing to guarantee the function terminates
  (x: int n)       // constrain the parameter x of type int by the sort n
  :<>              // where there are no side effects
  [m:int | m >= 0] // there exists an integer m which is greater than or equal to 0
  int m =          // constrain the return type by m
  if x = 0
     then 0        // this function will alway terminate and x must decrease each loop which
     else x + correct_sum_from_to_0 (x-1) // makes an incorrect implementation less likely

extern fn refined_sum_from_to_0 // we can go even further and force the output to be correct
  {n:int | n >= 0}              // by specifiyng the exact relation between the input and
  {m:int | m == (n*(n+1))/2}    // output of the function making it impossible to write
  (x: int n)                    // anything other than the correct implementation that passes
  :<>                           // typechecking. Implementation is left as a task for the
  int m                         // reader

datatype tree (a:t@ype, n:int) = // refinement types can also be used to keep track of the
  | leaf (a, 0)                  // size of a data structure where here the extra parameter
  | {x,y:int | x > 0; y > 0}     // represents the size at the type level
    branch (a, x+y) of (tree (a, x), a, tree (a, y))

sortdef nat = {n:int | n >= 0} // sortdef applys a constraint to a type and binds a name
sortdef pos = {n:int | n > 0}
sortdef neg = {n:int | n < 0}

fun traverse_tree                   // making sure we visit every node in a binary tree
  {a:t@ype} {n:nat} .<n>.           // of a type of unspecified size and a total size of n
  (t0: tree (a, n), fop: a -> void) // constrain tree to a type of a and size of n
  :<fun1>                           // with possible effects
  void =                            // and no return value
  case+ t0 of     // case+, case, and case- each handle errors differently with + treating all
  | leaf () => () // warnings as errors, - supressing all warnings, and without being the default
  | branch (l, v, r) => (
    traverse_tree (l, fop);
    fop(v);
    traverse_tree (r, fop);
    )

fun map_tree
  {a,b:t@ype} {n:nat} .<n>.           // with types a and b of unspecified size and for all n
  (t0: tree (a, n), fop: a -<> b)     // take a type tree constrained by a and n plus a pure function
  :<> tree (b, n) =                   // and return a tree of type b of the same size without effects
  case+ t0 of                         // note: `a -<> b` is a pure function without effects
  | leaf ()          => leaf () // if at a leaf, we're done
  | branch (l, v, r) =>         // if not we deconstruct the branch
    branch (map_tree (l, fop)   // then map fop over the left branch
           , fop(v)             // apply fop to the value of the current branch
           , map_tree (r, fop)) // and map fop over the right branch

// CASE STUDY: Generating Audio Streams ------------------------------------------ ATS

local
  staload "contrib/SDL2/SATS/SDL.sats"
in
end

// CASE STUDY: Expression Interpreter -------------------------------------------- ATS

local
  datatype Val =    // values can either be of type int or complex where each has
    | ValInt of int // it's own constructor
    | ValCom of complex

  datatype Ex =    // expressions can either be of a value or an operation with two
    | ExVal of Val // branches
    | ExMul of (Ex, Ex)
    | ExSub of (Ex, Ex)
    | ExAdd of (Ex, Ex)
    | ExDiv of (Ex, Ex)

  #define ExInt(x) ExVal (ValInt x) // for convenience we define macros for constructing
  #define ExCom(x) ExVal (ValCom x) // an ExVal directly to reduce noise

  fn ex_mul (x:Ex, y:Ex): Ex = ExMul (x,y) // constructors are lifted to the function level
  fn ex_sub (x:Ex, y:Ex): Ex = ExSub (x,y) // to allow being passed as an argument to a function
  fn ex_add (x:Ex, y:Ex): Ex = ExAdd (x,y)
  fn ex_div (x:Ex, y:Ex): Ex = ExDiv (x,y)

  extern fun mul_ex_ex (a: Ex, b: Ex): Ex // the interpreter supports the four operators + * / -
  extern fun sub_ex_ex (a: Ex, b: Ex): Ex // with each having it's own funciton
  extern fun add_ex_ex (a: Ex, b: Ex): Ex
  extern fun div_ex_ex (a: Ex, b: Ex): Ex
  extern fun rewrite(a: Ex): Ex // the result may not be in it's simplest form thus a rewrite
  extern fun eval (ex: Ex): Ex  // pass rewrites expressions such as (a+bi)/(c+di) into
                                // (a+c)/(bi+di) to allow for further reduction

  #define mac_match(a,b,c) a(b(ExInt i0, ExCom c0),c(ExInt i1, ExCom c1))
  #define mac_rewrite(a,b,c) a(ExInt (b), ExCom (c)) // several macros are defined for reducing
                                                     // the complexity for pattern matching and
  macdef mac_ex_ex (ty, a, b, ifop, cfop) =          // even to generate complete functions
    case+ (,(a), ,(b)) of
    | (ExInt i, ExInt j) => ExVal (ValInt (,(ifop) (i,j)))  // = a * b
    | (ExCom i, ExCom j) => ExVal (ValCom (,(cfop) (i,j)))  // = ai * bi
    | (ExCom c, ExInt i) => ,(ty) (ExInt i, ExCom c)        // = ai * b
    | (ExInt i, ExCom c) => ,(ty) (ExInt i, ExCom c)        // = a * bi
    | (a      , b      ) => ,(ty) (eval(a), eval(b))        // = (...) * (...)
                                                            // where * is any operator
                                                            // provided

  #define mac_ex(x,a,b) mac_ex_ex(x, a, b, multiply_int_int, multiply_complex_complex)

in

  implement eval (ex) = // eval is the core of our evaluator where we join all of the
  let                   // helper functions to handle the Ex expression passed
    overload * with mul_ex_ex // overloading common operators to further reduce complexity
    overload - with sub_ex_ex
    overload + with add_ex_ex
    overload / with div_ex_ex
  in
    rewrite(case+ ex of // rewrite the expression after evaluationg all sub expressions
    | ExVal v     => ExVal v // base case
    | ExMul (a,b) => a * b // each of the operators call eval to reduce the nested
    | ExSub (a,b) => a - b // expression until it hits a base case then return their
    | ExAdd (a,b) => a + b // result (mutual recursion)
    | ExDiv (a,b) => a / b)
  end

  implement rewrite (expression) = // rewriting joins both integers and complex numbers with the
    case+ expression of            // nested operator for all valid cases
    | mac_match(ExMul,ExMul,ExMul) => mac_rewrite(ExMul,i0*i1,c0*c1) // (a*bi)*(c*di) = (a*c)*(bi*di)
    | mac_match(ExSub,ExMul,ExMul) => mac_rewrite(ExMul,i0+i1,c0-c1) // (a*bi)-(c*di) = (a-c)*(bi-di)
    | mac_match(ExAdd,ExMul,ExMul) => mac_rewrite(ExMul,i0+i1,c0+c1) // (a*bi)+(c*di) = (a+c)*(bi*di)
                                                                     // (a*bi)/(c*di) = (a*bi)/(c*di)
    | mac_match(ExMul,ExAdd,ExAdd) => mac_rewrite(ExAdd,i0*i1,c0*c1) // (a+bi)*(c+di) = (a*c)+(bi*di)
    | mac_match(ExSub,ExAdd,ExAdd) => mac_rewrite(ExAdd,i0-i1,c0-c1) // (a+bi)-(c+di) = (a-c)+(bi-di)
    | mac_match(ExAdd,ExAdd,ExAdd) => mac_rewrite(ExAdd,i0+i1,c0+c1) // (a+bi)+(c+di) = (a+c)+(bi+di)
                                                                     // (a+bi)/(c+di) = (a+bi)/(c+di)
    | mac_match(ExMul,ExSub,ExSub) => mac_rewrite(ExSub,i0*i1,c0*c1) // (a-bi)*(c-di) = (a*c)-(bi*di)
    | mac_match(ExSub,ExSub,ExSub) => mac_rewrite(ExSub,i0+i1,c0+c1) // (a-bi)-(c-di) = (a+c)-(bi+di)
    | mac_match(ExAdd,ExSub,ExSub) => mac_rewrite(ExSub,i0+i1,c0+c1) // (a-bi)+(c-di) = (a+c)-(bi+di)
                                                                     // (a-bi)/(c-de) = (a-bi)/(c-di)
    | mac_match(ExMul,ExDiv,ExDiv) => mac_rewrite(ExDiv,i0*i1,c0*c1) // (a/bi)*(c/di) = (a*c)/(bi*di)
    | mac_match(ExSub,ExDiv,ExDiv) => mac_rewrite(ExDiv,i0-i1,c0-c1) // (a/bi)-(c/di) = (a-c)/(bi-di)
    | mac_match(ExAdd,ExDiv,ExDiv) => mac_rewrite(ExDiv,i0+i1,c0+c1) // (a/bi)+(c/di) = (a+c)/(bi+di)
                                                                     // (a/bi)/(c/di) = (a/bi)/(c/di)
    | general                      => general                        // ...           = ...

  implement mul_ex_ex (a, b) = mac_ex(ex_mul,a,b) // the implementation of evaluation is handled
  implement sub_ex_ex (a, b) = mac_ex(ex_add,a,b) // by a macro defined earlier making the
  implement add_ex_ex (a, b) = mac_ex(ex_add,a,b) // definition here as simple as passing the
  implement div_ex_ex (a, b) = mac_ex(ex_div,a,b) // arguments and a constructor
end
